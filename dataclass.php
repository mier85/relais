<?php

class dataClass
{
    protected $sqlConnection = null;
    
    public static function getDefault()
    {
        global $dbUserName, $dbPassword, $dbName;
        $class =  get_called_class();
        return new $class($dbUserName, $dbPassword, $dbName);
    }
    
    public function __construct($dbUserName, $dbPassword, $dbName)
    {
        $this->sqlConnection = new SQLite3('data.sql', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);
        $this->sqlConnection->exec("CREATE TABLE nodes(id INTEGER PRIMARY KEY, number INTEGER, name CHAR(255), backgroundOn CHAR(255), backgroundOff CHAR(255));");
        $nodes = $this->sqlConnection->query("SELECT * FROM nodes");
        if ($nodes->fetchArray() === false)
        {
            echo "creating";
            for ($i = 0; $i < 8; $i++)
            {
                $this->sqlConnection->exec("INSERT INTO nodes (number,name,backgroundOn,backgroundOff) VALUES ($i, \"\", \"\", \"\");");
            }
        }
    }
    
    public function __destruct() {
        $this->sqlConnection->close();
    }
    
    public function getSingleData($which)
    {
        $which = $this->sqlConnection->escapeString($which);
        $query = 'SELECT * FROM nodes WHERE number LIKE "'.$which.'"';
        $result = $this->sqlConnection->query($query);
        return $result !== false? $result->fetchArray() : null;
    }
    
    /**
     * print all buttons
     */
    public function getAll()
    {
        for($i = 0; $i < 8; $i++)
        {
            $data = $this->getSingleData($i);
            $attachAttributes='';
            $name = $i;
            if (!is_null($data))
            {
                if (!empty($data['name']))
                {
                    $name = $data['name'];
                }
                if (!empty($data['backgroundOn']))
                {
                    $attachAttributes.=' data-backgroundon="uploads/'.$data['backgroundOn'].'"';
                }
                if (!empty($data['backgroundOff']))
                {
                    $attachAttributes.=' data-backgroundoff="uploads/'.$data['backgroundOff'].'"';
                }
            }
            echo '<div id="but'.$i.'" class="toggle toggleCss img-rounded" style="vertical-align=middle; text-align:center" data-number="'.$i.'" '.$attachAttributes.'>'
                    .'<img id="img'.$i.'" src="" alt="'.$name.'" class="toggleButtonImage img-rounded">'.
                    '</div>';
        }
    }
    
    public static function filteredInput($name)
    {   
        return filter_input(INPUT_POST, $name);
    }
    
    public function setSingleData($which, $name, $on, $off)
    {
        $whichEsc = $this->sqlConnection->escapeString($which);        
        $nameEsc = $this->sqlConnection->escapeString($name);        
        $onEsc = $this->sqlConnection->escapeString($on);        
        $offEsc = $this->sqlConnection->escapeString($off);
        $setString = "name='$nameEsc'";
        $setString .= !empty($onEsc) ? ",backgroundOn='$onEsc'" : '';
        $setString .= !empty($offEsc) ? ",backgroundOff='$offEsc'" : '';
        return $this->sqlConnection->query("UPDATE nodes SET $setString WHERE number LIKE $whichEsc");
    }
    
    public function getError()
    {
        return $this->sqlConnection->error;
    }
    
    public function getRequest($var, $default = null)
    {
        return isset($_REQUEST[$var]) ? 
            $this->sqlConnection->escapeString($_REQUEST[$var]) : 
            $default;
    }
}