<?php
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');       
	if (!isset($_GET['action']))
	{
            return json_encode(false);
	}
	$action = $_GET['action'];
	$param = isset($_GET['param']) ? $_GET['param'] : '';
        include 'config/config.php';
        $relaisClass::execute($action, $param);

