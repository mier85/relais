<?php
    include_once 'config/config.php';
    if (isset($_GET['action']))
    {
        $data = new dataClass($dbUserName, $dbPassword, $dbName);
        switch ($_GET['action']){
            case 'all':
                $data->getAll();
                break;
            case 'single':
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');                
                if (!isset($_GET['which']))
                {
                    echo json_encode(false);
                } else
                {
                    $found = $data->getSingleData(urldecode($_GET['which']));
                    echo json_encode($data->getSingleData(urldecode($_GET['which'])));
                }
                break;
            default:
                break;                
        }
    }

