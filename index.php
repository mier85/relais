<?php
    include_once 'config/config.php';
    
    if (isset($_GET['controller']))
    {
        $controllerList = array('upload', 'buttons', 'relais');
        $controller = filter_input(INPUT_GET, 'controller');
        if (in_array($controller, $controllerList))
        {
            include $controller.".php";
            die();
        }
    }
?>
<!doctype html>
<html>
	<head>
		<title>remote control</title>
		<meta charset='utf-8'>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">                
		<script src="js/jquery-2.0.3.min.js"></script>
                <script src="js/jquery.mobile-1.3.2.min.js"></script>                
		<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="css/jquery.mobile-1.3.2.min.css" rel="stylesheet" media="screen">
		<script src="js/bootstrap.min.js"></script>
		<script src="js/main.js" type="text/javascript"></script>
		<link href="css/main.css" rel="stylesheet" media="screen">
	</head>
	<body>
                <div class="container mainDiv">
                    <div id="editDiv" data-edit='false' style="display:none; text-align:center;">
                        <h2>editor mode</h2>
                    </div>
                    <div id="toggleButtons">
                    </div>
                    <div class="smallMenu">
                        <div class="miniMenu img-rounded alert-info dropdown pull-right">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><div style="width:100%; height:100%;">menu</div></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li id="editFromMenu" class="editToggle"><a href="#" role="button" data-toggle="dropdown" class="editToggleA">edit</a></li>
                                <li><a href="#" role="button" data-toggle="dropdown" class="auto editToggleA" data-param="00000011" data-action="setPort">mirror lamps</a></li>
                                <li><a href="#" role="button" data-toggle="dropdown" class="auto editToggleA" data-param="00000000" data-action="setPort">turn off all</a></li>
                            </ul>                            
                        </div>
                    </div>
                    <div class="tools">
                            <div id="togboth" class="togbig img-rounded alert-info auto" data-action="setPort" data-param="00000011"></div>
                            <div id="offboth" class="togbig img-rounded alert-info auto" data-action="setPort" data-param="00000000"></div>
                            <div id="edit" class="editToggle togbig img-rounded alert-info">edit</div>
                            <div id="rawData" class="togbig img-rounded alert-info" style="float:left; overflow: auto; text-align:left"></div>
                            <div class="togbig img-rounded alert-info timer"></div>
                    </div>
                </div>
            <div id="editModal" class="modal hide editModal">
                <form>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Details</h3>
                    <div id="modalAlert"></div>                    
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td>Title:</td>
                            <td><input id="inputTitle" class="inputEdit" name="title" type="text" value="" /></td>
                        </tr>
                        <tr>
                            <td>Picture Off:</td>
                            <td>
                                <div id="editOff" class="toggleCss toggleEdit"><img src="" class="editButtonImage"></div>
                                <input name="images[off]" class="inputEdit" type="file" size="50" maxlength="100000" accept="image/*">                                
                            </td>
                        </tr>
                        <tr>
                            <td>Picture On:</td>
                            <td>
                                <div id="editOn" class="toggleCss toggleEdit"><img src="" class="editButtonImage"></div>
                                <input name="images[on]" class="inputEdit" type="file" size="50" maxlength="100000" accept="image/*">                                
                            </td>
                        </tr>
                        <input id="inputModalWhich" type="hidden" name="which" value="">
                    </table>
                </div>
                <div class="modal-footer">
                <progress id="editProgress" style="display:none;"></progress>
                <a href="#" class="btn" class="close" data-dismiss="modal" aria-hidden="true">Close</a>
                <a id="transmitEdit" href="#" class="btn btn-primary" data-loading-text="Uploading...">Save changes</a>
                </form>
                </div>                
            </div>
            <div id="timerModal" class="modal hide editModal">
                <form>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3>Timer</h3>
                        <div class="modalAlert"></div>
                    </div>
                    <div class="modal-body">
                        <div class="weekday-container" style="text-align:center; padding:15px;">
                            <div class="well weekday weekday-disabled">
                                <input type="hidden" name="day[0]" value="disabled">
                                sun
                            </div>
                            <div class="well weekday weekday-disabled">
                                <input type="hidden" name="day[1]" value="disabled">
                                mon
                            </div>
                            <div class="well weekday weekday-disabled">
                                <input type="hidden" name="day[2]" value="disabled">
                                tue
                            </div>
                            <div class="well weekday weekday-disabled">
                                <input type="hidden" name="day[3]" value="disabled">
                                wed
                            </div>
                            <div class="well weekday weekday-disabled">
                                <input type="hidden" name="day[4]" value="disabled">
                                thu
                            </div>
                            <div class="well weekday weekday-disabled">
                                <input type="hidden" name="day[5]" value="disabled">
                                fri
                            </div>
                            <div class="well weekday weekday-disabled">
                                <input type="hidden" name="day[6]" value="disabled">
                                sat
                            </div>
                        </div>
                        <div style="clear:both;">
                            <input class="span1" type="time" name="timer"  id="timer" placeholder="What time?"/>
                        </div>
                    </div>
                        <div class="modal-footer">
                            <a href="#" class="btn" class="close" data-dismiss="modal" aria-hidden="true">Close</a>
                            <a id="timerEditSave" href="#" class="btn btn-primary" data-loading-text="Uploading...">Save changes</a>
                        </div>                
                </form>
            </div>
	</body>
</html>
