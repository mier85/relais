#!/usr/bin/python
import serial


class RelayClass:
    """control relay"""

    def __init__(self, card=0):
        self.adr = card
        self.ser = self._init()

    def _init(self):
        ser = serial.Serial(port='/dev/ttyAMA0', baudrate=19200, timeout=1, bytesize=serial.EIGHTBITS, rtscts=False,
                            stopbits=serial.STOPBITS_ONE, parity=serial.PARITY_NONE)
        return ser

    def close(self):
        self.ser.close()

    def read_all(self):
        out = ''
        while self.ser.inWaiting() > 0:
            out += self.ser.read(1)
        return out

    def is_answer_sane(self, command, answer):
        if (ord(answer[0]) ^ ord(answer[1]) ^ ord(answer[2])) != ord(answer[3]):
            return False
        elif 255 - ord(answer[0]) != ord(command[0]):
            return False
        else:
            return True

    def init_and_resend(self, command):
        retry = range(20)
        for r in retry:
            if self.init_card() != False:
                return_code = self.send_command(command, False)
                if return_code != False:
                    print("suceed after "+str(r)+" times")
                    return return_code

        print("failed after trying for 20 times")
        return False

    def send_command(self, command, retry=True):
        written = self.ser.write(command)
        if 4 != written:
            print("error when sending command: " + str(written))

        answer = self.ser.read(4)
        if len(answer) != 4:
            print("received other amount than 4 characters: " + str(len(answer)))
            return

        if self.is_answer_sane(command, answer):
            return answer

        else:
            if ord(command[0]) != 1 and retry:
                return self.init_and_resend(command)

            return False

    def generate_command(self, command, data):
        command_string = chr(command) + chr(self.adr) + chr(data) + chr((command ^ self.adr ^ data))
        return command_string

    def _turn_off_single(self, number):
        return self.send_command(self.generate_command(6, 2 ** int(number)))

    def _turn_on_single(self, number):
        return self.send_command(self.generate_command(7, 2 ** int(number)))

    def set_single(self, number, state):
        if state:
            self._turn_on_single(number)
        else:
            self._turn_off_single(number)

    def get_all(self):
        ports = self.send_command(self.generate_command(2, 0))
        if ports != False:
            value = ''
            for i in range(8):
                value = value + ('1' if (ord(ports[2]) & 2 ** i == 2 ** i) else '0')
            return value
        else:
            return False

    def set_all(self, ports):
        if len(ports) != 8:
            return False
        command = 0
        counter = 0
        for c in ports:
            command |= (2 ** counter if (c == '1') else 0)
            counter += 1
        return self.send_command(self.generate_command(3, command))

    def toggle(self, number):
        return self.send_command(self.generate_command(8, 2 ** (int(number))))

    def init_card(self):
        self.send_command(self.generate_command(1, 0))

    def get_option(self):
        result = self.send_command(self.generate_command(4, 0))
        if not result:
            return ord(result[2])
        return False

    def set_option(self, option):
        try:
            if option < 0 or option > 3:
                print("option must be between 0 and 3")
                return False
        except TypeError:
            print("option must be a string")
            return False
        return self.send_command(self.generate_command(5, option))
