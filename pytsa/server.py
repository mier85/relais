import sys
import sqlite3

sys.path.append('.')
from functools import wraps
from flask import Flask, request, current_app, redirect, jsonify, abort, g
from RelayClass import RelayClass

app = Flask(__name__)
relay = RelayClass(1)

DATABASE = '../relay.db'

def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv

def get_db():
    db = getattr(Flask.g.top, '_database', None)
    if db is None:
        db = Flask._g.top._database = connect_db()

    db.row_factory = sqlite3.Row
    return db

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def support_jsonp(f):
    """Wraps JSONified output for JSONP"""

    @wraps(f)
    def decorated_function(*args, **kwargs):
        callback = request.args.get('callback', False)
        if callback:
            content = str(callback) + '(' + str(f(*args, **kwargs).data) + ')'
            return current_app.response_class(content, mimetype='application/javascript')
        else:
            return f(*args, **kwargs)

    return decorated_function


def get_number(number):
    try:
        return float(number)
    except ValueError:
        return "invalid"


def number_in_range_or_abort(number):
    numberFloat = get_number(number)
    if numberFloat == "invalid" or numberFloat < 0 or numberFloat > 7:
        abort(404)


@app.route('/')
def ok():
    return 'ok'

@app.route('/registerPush/<id>', methods=['GET'])
@support_jsonp
def register_push(id):
    cur = get_db().execute('INSERT OR IGNORE INTO push (uniqueId) VALUES (?)', (id))
    cur.close()
    return jsonify({"value": "ok"})


@app.route('/toggle/<number>', methods=['GET'])
@support_jsonp
def toggle(number):
    number_in_range_or_abort(number)
    relay.toggle(number)
    return jsonify({"value": relay.get_all()})


@app.route('/setSingle/<number>/<status>')
@support_jsonp
def set_single(number, status):
    relay.toggle(number)
    return jsonify({"value": relay.get_all()})


@app.route('/getAll')
@support_jsonp
def get_all():
    return jsonify({"value": relay.get_all()})


@app.route('/setAll/<value>')
@support_jsonp
def set_all(value):
    relay.set_all(value)
    return jsonify({"value": relay.get_all()})


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')
