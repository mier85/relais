<?php

class TimerClass extends dataClass{

    public function getThisMinute()
    {
        $time = date('H:i');
        $day = date('w');
        $currentTimeStamp = time();
        $query = "SELECT * FROM timer "
                ."WHERE time LIKE '$time' AND FIND_IN_SET('$day',day) > 0 AND "
                ."(repeatWeekly LIKE 'true' OR timestampplusseven > '$currentTimeStamp')";
        //echo $query.'\n\n';
        $result = $this->sqlConnection->query($query);
        $returnArray = array();
        if ($result !== false)
        {
            while ($row = $result->fetch_assoc())
            {
                $returnArray[] = $row;
            }
        }
                
        return $returnArray;
    }
    
    public function getNext()
    {
        
    }
    
    public function set($timeStamp)
    {
        $timestamp = $this->getRequest('timestamp');
    }
    
    public static function minutely()
    {
        global $relaisClass;
        $timer = self::getDefault();
        foreach($timer->getThisMinute() as $task)
        {
            $relaisClass::execute($task['action'], $task['param']);
        }
    }
}