/**
 * jQuery extension from : http://jsfiddle.net/ebEqu/
 * @param {type} param
 */
$.fn.extend({
    disableSelection: function() {
        this.each(function() {
            this.onselectstart = function() {
                return false;
            };
            this.unselectable = "on";
            $(this).css('-moz-user-select', 'none');
            $(this).css('-webkit-user-select', 'none');
        });
    }
});

window.interval = null;
window.showDebug = true;
function toggleDebug(event)
{
        var temp = (typeof event === 'boolean' ? event : !window.showDebug);
        if (temp)
            {
                window.showDebug = temp;
                logIt('toggling debug to: '+(temp ? 'turned on' : 'turned off'));
            } else
                {
                    logIt('toggling debug to: '+(temp ? 'turned on' : 'turned off'));
                    window.showDebug = temp;                                            
                }
        $("#rawData").removeClass(!window.showDebug ? 'alert-info' : 'alert-error');
        $("#rawData").removeClass(window.showDebug ? 'alert-error' : 'alert-info');
        $("#rawData").addClass(!window.showDebug ?  'alert-error' : 'alert-info' );
        $("#rawData").addClass(window.showDebug ? 'alert-info' : 'alert-error');
}

function logIt(message)
{
        if (window.showDebug === false)
        {
                return;
        }
    d = new Date();
    $("#rawData").append(d.toTimeString()+": "+message+"<br>");
    $("#rawData").scrollTop($("#rawData")[0].scrollHeight);
}

function refreshAll()
{
    logIt('trying to refresh all');
        $.ajax('?controller=relais&action=getPort', {
                type: "GET", 
                error: function(data, whatHappened)
                {
                    logIt('couldnt load ajax: '+whatHappened);                                                                                        
                },
                success: function(data)
                {
                        var result = data.result;
                        logIt('successfully loaded ajax of type: "'+(typeof data)+'"');
                        if (typeof data === 'object')
                        {
                            logIt('object data: '+JSON.stringify(data));
                        } else if (typeof data === 'string')
                            {                                                        
                                logIt('string: '+data);
                            }
                       if (result.length !== 8)
                        {
                            logIt("failed catching - default off");
                            result = '00000000';
                        } else
                            {
                                logIt("caught result: "+result);
                            }
                        for (var i=0; i < 8; i++)
                        {						
                                if ((result[i] === '1') && ($("#but"+(i)).hasClass('alert-success') === false))
                                {
                                        $("#but"+(i)).addClass('alert-success');
                                        $("#but"+(i)).removeClass('alert-error');
                                        var backgroundOn = $("#but"+(i)).data('backgroundon');
                                        if (typeof backgroundOn !== 'undefined')
                                        {
                                            $("#img"+(i)).attr('src',backgroundOn);
                                        }
                                } else if ((result[i] === '0') && ($("#but"+(i)).hasClass('alert-error') === false))
                                {
                                        $("#but"+(i)).addClass('alert-error');
                                        $("#but"+(i)).removeClass('alert-success');
                                        var backgroundOff = $("#but"+(i)).data('backgroundoff');
                                        if (typeof backgroundOff !== 'undefined')
                                        {
                                            $("#img"+(i)).attr('src',backgroundOff);
                                        }
                                }
                        }
                }
        });
}

function bindOnToggle()
{
    $('.toggle').on('click', function(){
                    $.ajax({
                        "url":"?controller=relais&action=toggle&param="+$(this).data('number'),
                        "success":function(data){
                                refreshAll();
                        }
                    })
            });
}

function bindEditOnToggle()
{
    $('.toggle').on('click', function(){
                editModal($(this).data('number'));
            });                            
}

function flushModal()
{                            
    $('#modalAlert').html();
    $('.inputEdit').each(function(key, item){
        $(item).val('');
    });
    $('img.toggleEdit').each(function(key, item){
        $(item).attr(src,'');
    });
}

function editModal(which)
{
    $.ajax({
        url:"?controller=buttons&action=single&which="+which, 
       success:function(data)
        {
            flushModal();
            $("#editModal").modal('show');
            $("#inputTitle").val(data.name == '' ? which : data.name);
            $('#editOff').find('img').attr('src',data.backgroundOff !== '' ? 'uploads/'+data.backgroundOff : '');
            $('#editOn').find('img').attr('src',data.backgroundOn  !== '' ? 'uploads/'+data.backgroundOn : '');
            $('#inputModalWhich').val(which);
        }
    });
}

function hideModal()
{
    $("#editModal").modal('hide');
}

function startInterval()
{
    stopInterval();
    window.interval = setInterval("refreshAll()",5000);                            
}

function stopInterval()
{
    if (window.interval !== null)
    {
        clearInterval(window.interval);
    }
}

function bindRightWayAndInterval()
{
    if ($("#editDiv").data("edit") === true)
    {
        $('.toggle').off('click');
        bindEditOnToggle();
        refreshAll();
        stopInterval();
    } else
        {
            $('.toggle').off('click');
            bindOnToggle();                                            
            refreshAll();
            startInterval();
        }
}

function loadToggleButtons()
{
    logIt('loading toggle buttons');
    $.ajax(
    {
        url:'?controller=buttons&action=all', 
        success:function(data)
        {
            $('#toggleButtons').html(data);
            bindRightWayAndInterval();
            updateOrInsertAllAutoNodes();
        }
    });
}                        

function insertOrUpdateNodes(element, param)
{
    var addIt = '';
    for (var i=0; i < 8; i++)
    {
        var node = $(element).find('node0'+i);
        if (node.length === 0)
        {
            var dataName = param[i] === '0' ? 'backgroundoff' : 'backgroundon';            
            addIt += '<div class="singleNode img-rounded node0'+i+' '+(param[i] === '0' ? 'nodeOff' : 'nodeOn' )+ '"><img class="img-rounded singleNodeImage" src="'+$('#but'+i).data(dataName)+'"/></div>';
        } else
        {
            $(node).addClass(param[i] === '0' ? 'nodeOff' : 'nodeOn');
            $(node).removeClass(param[i] === '0' ? 'nodeOn' : 'nodeOff');
            var dataName = param[i] === '0' ? 'backgroundoff' : 'backgroundon';
            $(node).find('.singleNodeImage').attr('src', $('#but'+i).data(dataName));
        }
    }
    $(element).html(addIt);
}

function updateOrInsertAllAutoNodes()
{
    $('.auto').each(function(key, data){
        insertOrUpdateNodes($(data), $(data).data("param"));
    });
}

$(document).ready(function(){				
        logIt('initial Load');
        loadToggleButtons();
        $('#rawData').on('click', toggleDebug);
        toggleDebug(false);
        $('.timer').disableSelection();
        $('.timer').bind('dblclick', function(){
           $('#timerModal').modal('show');
        });
        $('.timer').on('taphold', function(){
           $('#timerModal').modal('show');
        });
        $('.auto').each(function(key, data){
            $(data).bind('click', function()
            {
                var urlToSet = "?controller=relais&action="+$(data).data("action")+"&param="+$(data).data("param");
                $.ajax({
                        url:urlToSet,
                        success:function()
                        {
                            refreshAll();
                        }
                })
            });
        });
        $('.editToggle').on('click', function(){
            if ($("#editDiv").data("edit") == false)
            {
                $("#editDiv").data('edit',true);
                $("#editDiv").css('display', 'block');
                loadToggleButtons();
            } else
                {
                    $("#editDiv").data('edit', false);
                    $("#editDiv").css('display', 'none');
                        loadToggleButtons();
                }
            bindRightWayAndInterval();
        });

        $('.inputEdit:file').change(function(){
            var file = this.files[0];
            name = file.name;
            size = file.size;
            type = file.type;
            if (type !== 'image/jpeg' && type !== 'image/png')
            {
                return false;
            }
        });
        
        $('#timerModal').on('click', function(event){
            return false;
        });
        
        $('.weekday').disableSelection();
        $('.weekday').on('click', function(event){
            if ($(this).find('input').val() === 'disabled')
            {
                $(this).find('input').val('enabled');
                $(this).addClass('weekday-enabled');
                $(this).removeClass('weekday-disabled');
            } else
                {
                    $(this).find('input').val('disabled');
                    $(this).addClass('weekday-disabled');
                    $(this).removeClass('weekday-enabled');                    
                }
        });

    function beforeSendHandler()
    {
        $('#transmitEdit').button('loading');
        $('#editProgress').css('display','block');
        return true;
    }

    window.showAlert = function(typeClass, message)
    {
        $('#modalAlert').html('<div class="alert in '+typeClass+'"><pre>'+message+'</pre><a class="close" data-dismiss="alert" href="#">&times;</a></div>');                                
    }

    function completeHandler(data)
    {
        $('#transmitEdit').button('reset');
        $('#editProgress').css('display','none');
        if (data.success === true)
        {
            loadToggleButtons();
            hideModal();
        } else
            {
                window.showAlert('alert-error', data.message);
            }
    }

    function errorHandler()
    {
        $('#transmitEdit').button('reset');
        $('#editProgress').css('display','none');
        window.showAlert('alert-error', 'failed sending form');
    }

    $('#transmitEdit').click(function(){
        var formData = new FormData($('form')[0]);
        $.ajax({
            url: '?controller=upload',  //Server script to process data
            type: 'POST',
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){ // Check if upload property exists
                    myXhr.upload.addEventListener('#editProgress',progressHandlingFunction, false); // For handling the progress of the upload
                }
                return myXhr;
            },
            //Ajax events
            beforeSend: beforeSendHandler,
            success: completeHandler,
            error: errorHandler,
            // Form data
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false
        });
    });
    function progressHandlingFunction(e){
        if(e.lengthComputable){
            $('progress').attr({value:e.loaded,max:e.total});
        }
    }                            
});