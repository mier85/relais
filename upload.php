<?php
    include_once 'config/config.php';
    
    function codeToMessage($code)
    {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = "The uploaded file was only partially uploaded";
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = "No file was uploaded";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = "Missing a temporary folder";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = "Failed to write file to disk";
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = "File upload stopped by extension";
                break;

            default:
                $message = "Unknown upload error";
                break;
        }
        return $message;
    }
    
    function sendAnswerAndDie($success, $message)
    {
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 2050 05:00:00 GMT');
        header('Content-type: application/json');        
        echo json_encode(array('success' => $success, 'message' => $message));
        die();
    }

    /**
     * return file 
     * @param location $tmpFile
     */
    function cutAndTransformPic($filetype, $tmpFile, $filename, $maxX = 190, $maxY = 190)
    {
        list($width, $height, $image_type) = getimagesize($tmpFile);        
        switch ($image_type)
        {
            case IMAGETYPE_PNG:
                $imageRecieved = imagecreatefrompng($tmpFile);
                break;
            case IMAGETYPE_JPEG:
                $exif = exif_read_data($tmpFile);
                $imageRecieved = imagecreatefromjpeg($tmpFile);
                if(!empty($exif['Orientation'])) {
                    switch($exif['Orientation']) {
                        case 8:
                            $imageRecieved = imagerotate($imageRecieved,90,0);
                            break;
                        case 3:
                            $imageRecieved = imagerotate($imageRecieved,180,0);
                            break;
                        case 6:
                            $imageRecieved = imagerotate($imageRecieved,-90,0);
                            break;
                    }
                }                
                break;
            default:
                sendAnswerAndDie(false, 'unexpected file format: '.$filetype);
        }
        $newImage = imagecreatetruecolor($maxX, $maxY);
        $startX = 0;
        $startY = 0;
        $widthToTake = $width;
        $heightToTake = $height;
        // do we need to cut the picture
        if ($width > $height)
        {
            $startX = ($width - $height) / 2;
            $widthToTake = $height;
        } elseif ($height > $width)
        {
            $startY = ($height - $width) / 2; 
            $heightToTake = $width;
        }

        // was the transformation successful
        if (!imagecopyresized($newImage, $imageRecieved, 0, 0, $startX, $startY, $maxX, $maxY, $widthToTake, $heightToTake))
        {
            sendAnswerAndDie(false, 'error in transforming pic '.$filename);
        }
        return $newImage;
    }
    
    $which = dataClass::filteredInput('which');
    if (is_null($which) || !is_numeric($which) || ($which < 0) || ($which > 7))
    {
        sendAnswerAndDie(false, 'which not received or bad format');
    }
    
    $title = dataClass::filteredInput('title');
    if (is_null($title))
    {
        sendAnswerAndDie(false, 'no title received');
    }
    
    if (!isset($_FILES['images']))
    {
        sendAnswerAndDie(false, 'no files were transfered');
    }    

    $acceptedArrayKeys = array('on', 'off');
    // check for upload errors - dry
    foreach($_FILES['images']['error'] as $key => $error)
    {
        if (!in_array($key,$acceptedArrayKeys))
        {
            sendAnswerAndDie(false, 'bad index received');            
        }
        if ($error !== 0 && $error !== 4)
        {
            sendAnswerAndDie(false, codeToMessage($error));
        } elseif ($error === 0) { // check maximum file size
           if  ($_FILES['images']['size']['key'] > 5000000)
           {
                sendAnswerAndDie(false, 'file '.$_FILES['images']['name']['key'].' is too big. may only be 5mb big');
           }
        }
    }
    
    
    $filesToMove = array();
    // transform files - dry
    foreach($_FILES['images']['name'] as $key => $file)
    {
        if ($_FILES['images']['error'][$key] !== 4)
        {

            $newFileName = md5(uniqid('img').(string)microtime().$_FILES['name']['key']).'.png';
            $filesToMove[$key] = array('filename' => $newFileName, 'image' => cutAndTransformPic($_FILES['images']['type'][$key], $_FILES['images']['tmp_name'][$key],$_FILES['images']['name'][$key]));
        }
    }

    $background['on'] = '';
    $background['off'] = '';
    
    // now we really save the files
    foreach($filesToMove as $key => $file)
    {
        imagepng($file['image'], "uploads/".$file['filename'], 8);
        $background[$key] = $file['filename'];
    }
    $data = new dataClass($dbUserName, $dbPassword, $dbName);
    if (!$data->setSingleData($which, $title, $background['on'], $background['off']))
    {
        sendAnswerAndDie(false, 'sql error: '.$data->getError());
    }
    
    sendAnswerAndDie(true, 'everything is fine');