# Makefile
deployDir = /var/www/

permissions:
	sudo chown -R www-data:www-data $(deployDir)uploads/

deploy:
	sudo git archive --format=tar HEAD | (cd $(deployDir) && tar xf -)
	sudo mv $(deployDir)config/config.live.php $(deployDir)config/config.php
	sudo make permissions
	sudo rm $(deployDir)Makefile
	sudo rm -rf $(deployDir)tests

update:
	git pull --rebase

deploy-latest: update deploy
    
