#!/usr/bin/python
import sys
import serial

def init():
	ser = serial.Serial(port='/dev/ttyAMA0', baudrate=19200, timeout=1, bytesize=serial.EIGHTBITS, rtscts=False, stopbits=serial.STOPBITS_ONE, parity=serial.PARITY_NONE)
	return ser

def close(ser):
	ser.close()

def readAll(ser):
	out = ''
	while ser.inWaiting() > 0:
		out += ser.read(1);
	return out

def checkIfAnswerIsSane(command, answer):
	if ((ord(answer[0]) ^ ord(answer[1]) ^ ord(answer[2])) != ord(answer[3])):
		return False;
	if (255 - ord(answer[0]) != ord(command[0])):
		return False;
	return True;

def initAndResend(ser, command):
	retry = range(10)
	for r in retry:
		if ((initCard(ser, ord(command[1])) != False)):
			returnCode = sendCommand(ser, command, False)
			if (returnCode != False):
				return returnCode
	print("failed after trying for 10 times")
	return False

def sendCommand(ser, command, retry=True):
	written = ser.write(command)
	if written != 4:
		print 'error when sending command'
	answer = ser.read(4)
	if len(answer) != 4:
		print 'received other amount than 4 characters: '+str(len(answer))
		return
	if (checkIfAnswerIsSane(command, answer)):
		return answer;
	else:
		if ((ord(command[0]) != 1) and (retry == True)):
			return initAndResend(ser, command)
		return False;

def generateCommand(command, adress, data):
	commandString = chr(command) + chr(adress) + chr(data) + chr((command ^ adress ^ data))
	return commandString

def turnOnSingle(ser, adr, number):
	return sendCommand(ser, generateCommand(6, adr, 2**int(number)))

def turnOffSingle(ser, adr, number):
	return sendCommand(ser, generateCommand(7, adr, 2**int(number)))

def getPort(ser, adr):
	ports = sendCommand(ser, generateCommand(2, adr, 0))
	if (ports != False):
		str = ''
		for i in range(8):
			str = str + ('1' if (ord(ports[2]) & 2**i == 2**i) else '0')
		return str
	else:
		return False	

def setPort(ser, adr, ports):
	if (len(ports) != 8):
		return False
	command = 0; 
	counter = 0;
	for c in ports:		
		command = command | (2**counter if (c=='1') else 0)
		counter += 1
	return sendCommand(ser, generateCommand(3, adr, command))  

def toggle(ser, adr, number):
	return sendCommand(ser, generateCommand(8, adr, 2**(int(number))))

def initCard(ser, adr):
#	if (sendCommand(ser, generateCommand(0, 0, 0)) == False):
#		print("attempting to initialize..")
	sendCommand(ser, generateCommand(1, adr, 0))
#	ser.read(4)
#	ser.read(4)
#	sendCommand(ser, generateCommand(1, 0, 0))
#	ser.read(4);
#	else:
#		return True;

def getOption(ser,adr):
	result =  sendCommand(ser, generateCommand(4,adr,0))
	if (result != False):
		return ord(result[2])
	return False

def setOption(ser,adr,option):
	try:
		if (option < 0 or option > 3):
			print("option must be between 0 and 3")
			return False
	except TypeError:
		print("option must be a string")
		return False;
	return sendCommand(ser, generateCommand(5,adr,option))


if (len(sys.argv) == 1):
	print('no arguments received')
	sys.exit(1)
	
ser = init()
initCard(ser, 0)

if (len(sys.argv) == 3):
	result = locals()[sys.argv[1]](ser, 1, sys.argv[2])
else:
	result = locals()[sys.argv[1]](ser, 1)
#
if (result == False):
	print("failed")
	sys.exit(3)
else:
	print(result)
close(ser)
